<?php include 'functions.php';

$html='';
$status=false;
$attachment=array();
$website=(isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . preg_replace('@/+$@', '', dirname($_SERVER['SCRIPT_NAME'])) . '/';


if(isset($_POST['company_information'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Company Information</h5>";
  foreach ($_POST['company_information'] as $key => $value) {
  	 $html.="<strong>".decode_key($key)." : </strong>".$value."<br>";
  }
}

if(isset($_POST['company_registration_details'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Company Registration Details</h5>";
  foreach ($_POST['company_registration_details'] as $key => $value) {
  	 $html.="<strong>".decode_key($key)." : </strong>".$value."<br>";
  }
}

if(isset($_POST['business_profile'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Business Profile</h5>";
  foreach ($_POST['business_profile'] as $key => $value) {
  	 $html.="<strong>".decode_key($key)." : </strong>".$value."<br>";
  }
}

if(isset($_POST['primary_market_segments'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Primary Market Segments</h5>";
  $bptemp=array();
  foreach ($_POST['primary_market_segments'] as $key => $value) {
  	 $bptemp[]=decode_key($value);
  }
  $html.=implode(', ', $bptemp);
}


if(isset($_POST['revenue_split_by_industry'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Revenue Split by Industry</h5>";
  foreach ($_POST['revenue_split_by_industry'] as $key => $value) {
  	 $html.="<strong>".decode_key($key)." : </strong>".$value."<br>";
  }
}

if(isset($_POST['existing_vendor_relationships']) && !empty($_POST['existing_vendor_relationships'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Existing Vendor Relationships (including any Mailing vendors) with % of revenue contribution</h5>";
  $html.="<table border='1'>";


  $html.="<thead>";
  $html.="<tr>";
	  $html.="<th>Vendor Name</th>";
	  $html.="<th>Revenue Contribution %</th>";
	  $html.="<th>Partner Since</th>";
	  $html.="<th>Partnership Level</th>";
  $html.="</tr>";
  $html.="</thead>";

  $html.="<tbody>";
	  foreach ($_POST['existing_vendor_relationships'] as $key => $value) {
	  	if(!empty($value)){
	  	$header=array_keys($_POST['existing_vendor_relationships'][$key]);
	  	$html.="<tr>";
		  	foreach ($header as $inkey => $invalue) {		
		  		$html.="<td>".decode_key($value[$invalue])."</td>";	
		  	}	
	  	$html.="</tr>";  	
	  }}
 
  $html.="</tbody>";

  $html.="</table>";
}


if(isset($_POST['top_10_customer_insights']) && !empty($_POST['top_10_customer_insights'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Top 10 Customer Insights</h5>";
  $html.="<table border='1'>";


  $html.="<thead>";
  $html.="<tr>";
	  $html.="<th>Name of Customer</th>";
	  $html.="<th>Customer Since</th>";
	  $html.="<th>Solution Sold</th>";
  $html.="</tr>";
  $html.="</thead>";

  $html.="<tbody>";
	  foreach ($_POST['top_10_customer_insights'] as $key => $value) {
	  	if(!empty($value)){
	  	$header=array_keys($_POST['top_10_customer_insights'][$key]);
	  	$html.="<tr>";
		  	foreach ($header as $inkey => $invalue) {		
		  		$html.="<td>".decode_key($value[$invalue])."</td>";	
		  	}	
	  	$html.="</tr>";  	
	  }}
 
  $html.="</tbody>";

  $html.="</table>";
}


if(isset($_POST['team_insights'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Team Insights</h5>";
  foreach ($_POST['team_insights'] as $key => $value) {
  	 $html.="<strong>".decode_key($key)." : </strong>".$value."<br>";
  }
}


if(isset($_POST['contact_information']) && !empty($_POST['contact_information'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Contact Information</h5>";
  $html.="<table border='1'>";


  $html.="<thead>";
  $html.="<tr>";
	  $html.="<th>Contact Type</th>";
	  $html.="<th>Email</th>";
	  $html.="<th>Mobile Number</th>";
  $html.="</tr>";
  $html.="</thead>";

  $html.="<tbody>";
	  foreach ($_POST['contact_information'] as $key => $value) {
	  	if(!empty($value)){
	  	$header=array_keys($_POST['contact_information'][$key]);
	  	$html.="<tr>";
		  	foreach ($header as $inkey => $invalue) {
		  	    if($inkey==0){
		  	    	$html.="<td><strong style='color: #1a9fff;'>".decode_key($invalue).": </strong>".decode_key($value[$invalue])."</td>";	
		  	    }else{
		  	    	$html.="<td>".decode_key($value[$invalue])."</td>";	
		  	    }		
		  		
		  	}	
	  	$html.="</tr>";  	
	  }}
 
  $html.="</tbody>";

  $html.="</table>";
}


if(isset($_POST['icewarp_relationship_interest'])){
  $html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>IceWarp Relationship Interest</h5>";
  $bptemp=array();
  foreach ($_POST['icewarp_relationship_interest'] as $key => $value) {
  	 $bptemp[]=decode_key($value);
  }
  $html.=implode(', ', $bptemp);
}


$html.="<h5 style='color: #0071c3;font-size: 22px;font-weight: 600;'>Uploaded Documents</h5>";
if(isset($_FILES) and !empty($_FILES)){
	foreach ($_FILES as $key => $value) {
		if(isset($_FILES[$key]) and !empty($_FILES[$key]['name'])){
		  $file=upload($key,'documents');
		  if($file['status']){
		  	$uploadedFile=$file['name'];
		  	$attachment[]="documents/$uploadedFile";
            $html.="<strong style='color: #1a9fff;'>".decode_key($key).": </strong><a href='https://mailers.icewarp.me/partner_program/documents/$uploadedFile'>https://mailers.icewarp.me/partner_program/documents/$uploadedFile</a><br>";	
		  }          
        }
	}
	
}


if(isset($_POST['user_info'])){
  foreach ($_POST['user_info'] as $key => $value) {
  	 $html.="<strong>".decode_key($key)." : </strong>".$value."<br>";
  }
}




$email=array(
	'address'=>array(
		array('name'=>'Granular Market','email'=>'marketing@icewarp.me'),
		array('name'=>'Anita Kukreja','email'=>'anita.kukreja@icewarp.co.in'),
		array('name'=>'Icewarp Marketing','email'=>'marketing@icewarp.co.in')
	),
	'subject'=>'IceWarp Enquiry For Partner Program',
	'message'=>$html,
	'attachment'=>$attachment
);
$status= mailer($email);

if($status){	

		$userEmail=array(
			'address'=>array(
				array('name'=>$_POST['company_information']['contact_person_name'],'email'=>$_POST['company_information']['email_id'])
			),
			'subject'=>'IceWarp Enquiry For Partner Program',
			'message'=>file_get_contents("thankyoumsg.html")
		);

		if(isset($_POST['send_me_a_copy']) && $_POST['send_me_a_copy']=='on')
		{
		  $pdfData=array(
	         "html"       =>$html,
	         "path"       =>"tmp/"
		  );              

		  $pdf=pdf_export($pdfData);
		  $userEmail['attachment']="tmp/$pdf";
		}

		mailer($userEmail);
		echo "<script> window.location.href='https://mailers.icewarp.me/partner_program/thankyou.html'</script>";
}else{
	echo "<script> alert('Failed: please try again!'); window.location.href='https://mailers.icewarp.me/partner_program'</script>";
}

?>