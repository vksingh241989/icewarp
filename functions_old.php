<?php 

function encode_key($value='')
{

	$value=str_replace(array('/'), '_/_', $value);
	$value=str_replace(array(' '), '_', $value);
	$value=strtolower($value);
	return $value;

}

function decode_key($value='')
{   
    $value=str_replace(array('_'), ' ', $value);
	$value=ucwords($value);
	$value=str_replace(array(' / '), '/', $value);
	return $value;

}


function upload($name,$location)
{
	
		$filelocation=$_FILES[$name]['tmp_name'];
		$ext = pathinfo($_FILES[$name]['name'], PATHINFO_EXTENSION);
		$file=date('dmyhis').rand(10,99).".".$ext;
		if(move_uploaded_file($filelocation,"$location/$file")){
			return array('status'=>true,'name'=>$file);
		}else{
			return array('status'=>false,'name'=>'');
		}

}


   function mailer($data=array()){
		
	    if (!class_exists('PHPMailer')) {
	      require_once "phpmailer/class.phpmailer.php";
          $mail = new PHPMailer();
        }  

		try {
			    $status=0;
		        $fromMail= 'no-reply@groupgfr.com';
		        $fromName= 'IceWarp';
                $mail = new PHPMailer();  
		        $mail->IsSMTP();                               // Set mailer to use SMTP
		        $mail->SMTPAuth = true;                        // Enable SMTP authentication
		        $mail->SMTPSecure = '';                     // Enable encryption, 'tls' also accepted
		        $mail->Host       ='groupgfr.com';       // Specify main and backup server
		        $mail->Port       = '587';                             // Set the SMTP port		       
		        $mail->Username   = 'no-reply@groupgfr.com';   // SMTP username
		        $mail->Password   = 'gjBS4Pk{C4#P';            // SMTP password
		         $mail->Encoding   = '7bit';
		        $mail->SMTPDebug  = 0;

		        $mail->clearAllRecipients();
				$mail->ClearAddresses();
				$mail->ClearAttachments();

		        $mail->SetFrom($fromMail,$fromName);
		        $mail->AddReplyTo($fromMail,$fromName);        // Add recipient who get reply of mail

		        if(isset($data['cc']) && !empty($data['cc'])){
		          $mail->addCC($data['cc']);                // Add cc recipient
		        }
		        if(isset($data['bcc']) && !empty($data['bcc'])){
		          $mail->addBCC($data['bcc']);              // Add bcc recipient
		        }
		        if(isset($data['subject']) && !empty($data['subject'])){
		          $mail->Subject = $data['subject'];                    // Add mail subject
		        }
		        if(isset($data['message']) && !empty($data['message'])){
		          $mail->MsgHTML($data['message']);             // Set mail content, html or plain
		        }
		        if(isset($data['altBody']) && !empty($data['altBody'])){
		          $mail->AltBody = $data['altBody'];
		        }
		        if(isset($data['attachment']) && !empty($data['attachment'])){
		        	if(is_array($data['attachment'])){
			        	foreach ($data['attachment'] as $key => $value) {
			        		$mail->AddAttachment($value);    
			        	}
			        }else{
			        	    $mail->AddAttachment($data['attachment']); 
			        }			                 
		        }    

		        foreach ($data['address'] as $key => $value) {
		            $mail->AddAddress($value['email'], $value['name']);      // Add a recipient, name is optional
		        }                
		        
		        $result = $mail->Send();                       //Send mail
		        $message = $result ? $status=true: $status=false;     //Get mail status

		        return $status;

		} catch (Exception $ex) {
			 $ex->getMessage();
		}
	}


	function pdf_export($data) {  
            
                 $param = NULL;
                 require_once "mpdf/vendor/autoload.php";
                                if ($param == NULL) {
                                    $param = '"en-GB-x","A4","","",10,10,10,10,6,3';
                                }
                              $mpdf = new \Mpdf\Mpdf();
                              $filenName ="pdf-".time().".pdf";
                              $mpdf->SetFooter('<a href="'.$_SERVER['HTTP_HOST'].'">'.$_SERVER['HTTP_HOST'].'</a>'.'|{PAGENO}|'.date(DATE_RFC822));
                              if(!empty($data['stylesheet'])){
                                      foreach ($data['stylesheet'] as $key => $value) {
                                        $mpdf->WriteHTML(file_get_contents($value),1);
                                      }
                              } 
                              $mpdf->SetWatermarkText("IceWarp");
                              $mpdf->showWatermarkText = true;         
                              $mpdf->WriteHTML($data['html'],2);
                              if(isset($data['path']) AND !empty($data['path'])){
                                   $mpdf->Output($data['path'].$filenName, 'F');       
                                   return $filenName;                                                
                                }else{
                                   $mpdf->Output($filenName, 'D');   
                              }      
       }


?>