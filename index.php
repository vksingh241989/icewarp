<?php include 'functions.php'; ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Partner Form</title>

    <link rel="canonical" href="">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/form-validation.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


  </head>

  <body class="top_box ">
<!--     <div class="row top_img">
       
           <img src="img/header_logo.png" class="img-fluid" alt="Responsive image">
         
    </div> -->
    <div class="container-fluid wrapper text_div">
      <div class="text_div_p">
        <p class="text-left text-dark"><h3>Partner Application</h3><br>

Please fill out the application form and accept the IceWarp Partner Program Terms and Conditions. Begin by entering all the information below.</p>
      </div>
         
      

      <form action="apply.php" method="POST" enctype="multipart/form-data">
        <div class="text_blue mt-4 mb-3">Company Information</div>
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Legal Company Name<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[company_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Company Name" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Address<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[address]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Address" required="">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">City<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[city]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="City" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Country<span class="required">*</span></label>
            <div class="col-sm-4">
              <!-- <input type="text"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="Country" required=""> -->
                                         <select class="form-control" name="company_information[country]" required="">
                                                <option value="">Select Country</option>
                                                <option value="Afghanistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antarctica">Antarctica</option>
                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bolivia">Bolivia</option>
                                                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Bouvet Island">Bouvet Island</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Central African Republic">Central African Republic</option>
                                                <option value="Chad">Chad</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Christmas Island">Christmas Island</option>
                                                <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Democratic Republic of the Congo">Democratic Republic of the Congo</option>
                                                <option value="Cook Islands">Cook Islands</option>
                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Cote D`Ivoire (Ivory Coast)">Cote D`Ivoire (Ivory Coast)</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Republic">Dominican Republic</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France</option>
                                                <option value="French Guiana">French Guiana</option>
                                                <option value="French Polynesia">French Polynesia</option>
                                                <option value="French Southern Territories">French Southern Territories</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam</option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Heard and McDonald Islands">Heard and McDonald Islands</option>
                                                <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran">Iran</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="North Korea">North Korea</option>
                                                <option value="South Korea">South Korea</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Laos">Laos</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libya">Libya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macau">Macau</option>
                                                <option value="Macedonia">Macedonia</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mayotte">Mayotte</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Micronesia">Micronesia</option>
                                                <option value="Moldova">Moldova</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Namibia">Namibia</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Netherlands">Netherlands</option>
                                                <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Niue">Niue</option>
                                                <option value="Norfolk Island">Norfolk Island</option>
                                                <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palau">Palau</option>
                                                <option value="Palestinian territories">Palestinian territories</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Philippines">Philippines</option>
                                                <option value="Pitcairn">Pitcairn</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Reunion">Reunion</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russian Federation">Russian Federation</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="Saint Helena">Saint Helena</option>
                                                <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                <option value="Saint Lucia">Saint Lucia</option>
                                                <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                <option value="Saint Vincent &amp; the Grenadines">Saint Vincent &amp; the Grenadines</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia">Serbia</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore</option>
                                                <option value="Slovak Republic">Slovak Republic</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Somalia">Somalia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Svalbard &amp; Jan Mayen Islands">Svalbard &amp; Jan Mayen Islands</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>
                                                <option value="Syrian">Syrian</option>
                                                <option value="Taiwan">Taiwan</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania">Tanzania</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Timor-Leste">Timor-Leste</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Tokelau">Tokelau</option>
                                                <option value="Tonga">Tonga</option>
                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="United Arab Emirates">United Arab Emirates</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="United States">United States</option>
                                                <option value="US Minor Outlying Islands">US Minor Outlying Islands</option>
                                                <option value="Uruguay">Uruguay</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Vietnam">Vietnam</option>
                                                <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                                <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                <option value="Western Sahara">Western Sahara</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                                <option value="Montenegro">Montenegro</option>
              </select>
            </div>
          </div>
          <!-- end form group -->
            <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Company Telephone<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[company_telephone]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Company Telephone" pattern="^[0-9+]+$" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Company Email<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="email" name="company_information[company_email]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Company Email" pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$" required="">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Website URL<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[website_url]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Website URL" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Number of Locations </label>
            <div class="col-sm-4">
              <input type="text" name="company_information[number_of_locations]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Number of Locations">
            </div>
          </div>
          <!-- end form group -->
                    <!-- form group -->
         <!--  <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">if more than one </label>
            <div class="col-sm-4">
              <input type="text" name="company_information[specify]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Please specify">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Other </label>
            <div class="col-sm-4">
              <input type="text" name="company_information[sister_organizations]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Sister Organizations">
            </div>
          </div> -->
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Contact Person Name<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[contact_person_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Contact Person Name" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Mobile Number<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$" required="">
            </div>
          </div>
          <!-- end form group -->
            <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Designation<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="text" name="company_information[designation]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Designation" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Email ID<span class="required">*</span></label>
            <div class="col-sm-4">
              <input type="email" name="company_information[email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID" pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$" required="">
            </div>
          </div>
          <!-- end form group -->
          <div class="text_blue mt-4 mb-3">Company Registration Details</div>
             <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Trade License / Company Registration Number</label>
            <div class="col-sm-4">
              <input type="text" name="company_registration_details[trade_license_/_company_registration_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Trade License / Company Registration Number">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Date of Issue </label>
            <div class="col-sm-4">
              <input type="date" name="company_registration_details[date_of_issue]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Date of Issue ">
            </div>
          </div>
          <!-- end form group -->
             <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Expiry Date</label>
            <div class="col-sm-4">
              <input type="date" name="company_registration_details[expiry_date]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Expiry Date">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Tax ID Number (if any) </label>
            <div class="col-sm-4">
              <input type="text" name="company_registration_details[tax_id_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Tax ID Number (if any) ">
            </div>
          </div>
          <!-- end form group -->
          <div class="text_blue mt-4 mb-3">Business Profile</div>
          <p class="font-18">Fiscal Year Start</p>
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Annual Revenue (Last Year)</label>
            <div class="col-sm-4">
              <input type="text" name="business_profile[annual_revenue_(last_year)]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="in USD only">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Expected Annual Revenue (This Year)</label>
            <div class="col-sm-4">
              <input type="text" name="business_profile[expected_annual_revenue_(this_year)]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="in USD only">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-3 col-form-label col-form-label-sm">Revenue for Mailing Solutions (if any)</label>
            <div class="col-sm-4">
              <input type="text" name="business_profile[revenue_for_mailing_solutions_(if_any)]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="in USD only">
            </div>
          </div>
          <!-- end form group -->

          <div class="text_blue mt-4 mb-3">Primary Market Segments</div>
          <!-- form group -->
          <div class="form-group">
            <div class="form-check mb-2">
              <input class="form-check-input" name="primary_market_segments[]" value="smb_(less_than_100_employees)" type="checkbox" id="gridCheck">
              <label class="form-check-label " for="gridCheck">
                SMB (less than 100 employees)
              </label>
            </div>
             <!-- end form group -->
            <!-- form group -->
             <div class="form-group">
            <div class="form-check mb-2">
              <input class="form-check-input" name="primary_market_segments[]" value="mid_market(between_101_to_1000_employees)" type="checkbox" id="gridCheck">
              <label class="form-check-label" for="gridCheck">
                Mid-Market (between 101 to 1000 employees)
              </label>
            </div>
             <!-- end form group -->
            <!-- form group -->
             <div class="form-group mb-2">
            <div class="form-check mb-2">
              <input class="form-check-input" name="primary_market_segments[]" value="enterprise_(1,000+_employees)" type="checkbox" id="gridCheck">
              <label class="form-check-label" for="gridCheck">
                Enterprise (1,000+ Employees)
              </label>
            </div>
             <!-- end form group -->

          <div class="text_blue mt-4 mb-3">Revenue Split by Industry</div>
             <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Banking/Finance</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[banking_/_finance]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Banking/Finance">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Construction </label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[construction]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Construction">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Manufacturing</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[manufacturing]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Manufacturing">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">IT/ITeS</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[it_/_ites]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="IT/ITeS">
            </div>
          </div>
          <!-- end form group -->
            <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Telecom/ISP</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[telecom_/_isp]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Telecom/ISP">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Education</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[education]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Education">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Healthcare</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[healthcare]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Healthcare">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Hospitality</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[hospitality]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Hospitality">
            </div>
          </div>
          <!-- end form group -->
                    <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Retail</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[retail]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Retail">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Government/Public Sector</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[government_/_public_sector]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Government/Public Sector">
            </div>
          </div>
          <!-- end form group -->
          <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Others</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[others]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Please specify">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Mobile Number</label>
            <div class="col-sm-4">
              <input type="text" name="revenue_split_by_industry[mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
          </div>
          <!-- end form group -->

          <div class="text_blue mt-5 mb-3">Existing Vendor Relationships (including any Mailing vendors) with % of revenue contribution</div>

          <!--form group -->
          <?php for ($i=1; $i <=6; $i++) {  ?>
          <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="existing_vendor_relationships[<?=$i-1?>][vendor_name_<?=$i?>]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Vendor Name <?= $i ?>">
            </div>
            <div class="col-sm-3">
              <input type="text" name="existing_vendor_relationships[<?=$i-1?>][revenue_contribution_%]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Revenue Contribution % ">
            </div>
            <div class="col-sm-3">
              <input type="text" name="existing_vendor_relationships[<?=$i-1?>][partner_since]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Partner Since">
            </div>
            <div class="col-sm-3">
              <input type="text" name="existing_vendor_relationships[<?=$i-1?>][partnership_level]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Partnership Level">
            </div>
          </div>
        <?php } ?>
           
           <!-- end form group -->

          <div class="text_blue mt-5 mb-3">Top 10 Customer Insights</div>

            <!--form group -->
        <?php for ($i=1; $i <=10 ; $i++) {  ?>
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="top_10_customer_insights[<?=$i-1?>][name_of_customer<?=$i?>]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Name of Customer <?=$i?>">
            </div>
            <div class="col-sm-3">
              <input type="text" name="top_10_customer_insights[<?=$i-1?>][customer_since]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Customer Since">
            </div>
            <div class="col-sm-3">
              <input type="text" name="top_10_customer_insights[<?=$i-1?>][solution_sold]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Solution Sold">
            </div>
           </div>
        <?php } ?>
           <!-- end form group -->

          <div class="text_blue mt-5 mb-3">Team Insights</div>
          <p>Total Number of Employees</p>
             <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">in Sales & Marketing  </label>
            <div class="col-sm-4">
              <input type="text" name="team_insights[in_sales_&_marketing]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="in Sales & Marketing  ">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">in Technical </label>
            <div class="col-sm-4">
              <input type="text" name="team_insights[in_technical]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="in Technical">
            </div>
          </div>
          <!-- end form group -->
             <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">in Administration </label>
            <div class="col-sm-4">
              <input type="text" name="team_insights[in_administration]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="in Administration">
            </div>
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Others</label>
            <div class="col-sm-4">
              <input type="text" name="team_insights[others]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Others">
            </div>
          </div>
          <!-- end form group -->
         

          <div class="text_blue mt-5 mb-3">Contact Information</div>
          <p>Contact Type </p>
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[0][owner_/_md_/_ceo_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Owner/MD/CEO Name">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[0][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[0][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[1][sales_head_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Sales Head Name">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[1][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[1][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[2][sales_manager_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Sales Manager Name">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[2][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[2][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[3][presales_manager_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Presales Manager Name">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[3][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[3][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[4][technical_head_name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Technical Head Name">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[4][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[4][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[5][marketing_contact]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Marketing Contact">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[5][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[5][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <!--form group -->
           <div class="form-group row">
            <div class="col-sm-3">
              <input type="text" name="contact_information[6][finance_contact]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Finance Contact">
            </div>
            <div class="col-sm-3">
              <input type="email" name="contact_information[6][email_id]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Email ID " pattern="[a-zA-Z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,30}$">
            </div>
            <div class="col-sm-3">
              <input type="text" name="contact_information[6][mobile_number]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Mobile Number" pattern="^[0-9+]+$">
            </div>
           </div>
           <!-- end form group -->
            <div class="text_blue mt-4 mb-3">IceWarp Relationship Interest</div>
          <!-- form group -->
          <div class="form-group">
            <div class="form-check mb-2">
              <input class="form-check-input" name="icewarp_relationship_interest[]" value="sales_only" type="checkbox" id="gridCheck">
              <label class="form-check-label " for="gridCheck">
                 Sales Only
              </label>
            </div>
             <!-- end form group -->
            <!-- form group -->
             <div class="form-group">
            <div class="form-check mb-2">
              <input class="form-check-input" name="icewarp_relationship_interest[]" value="sales,_implementation_&_support" type="checkbox" id="gridCheck">
              <label class="form-check-label" for="gridCheck">
               Sales, Implementation & Support
              </label>
            </div>
             <!-- end form group -->

            <div class="text_blue mt-5 mb-3">Territory (Country) request</div>
                    <!-- form group -->
          <div class="form-group row">
            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Please specify</label>
            <div class="col-sm-4">
              <input type="text" name="territory_request[territory_/_country]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Territory/Country">
            </div>            
          </div>
          <!-- end form group -->
          <div class="text_blue mt-5">Attachments</div>
             <!-- row -->
              <div class="row mt-2">
                <div class="col-md-3">
                <label for="file-upload" class="custom-file-upload">Company Registration Document<span class="required">*</span></label>
              </div>
              <div class="col-md-6">
                <input id="file-upload" name="company_registration_document" type="file" required="">
              </div>
              </div>

              <!-- end row -->
               <!-- row -->
              <div class="row mt-2">
                 <div class="col-md-3">
                  <label for="file-upload" class="custom-file-upload">Tax Certificate<span class="required">*</span></label>
                </div>
                <div class="col-md-6">
                  <input id="file-upload" name="tax_certificate" type="file" required="">
                </div>
              </div>

              <!-- end row -->
               <!-- row -->
              <div class="row mt-2">
                <div class="col-md-3">
                  <label for="file-upload" class="custom-file-upload">Any Other</label>
                </div>
                <div class="col-md-6">
                  <input id="file-upload" name="any_other" type="file">
                </div>
              </div>
              <!-- end row -->
            <!-- end Attachments -->
            <!-- term & condition -->
             <div class="row mt-4">
              <div class="col-md-12">
                <div  class="checkbox">
                  <label><input type="checkbox" name="terms_&_condition" value="I confirm that the above provided information is accurate and errors, if any, are mine." required="">&nbsp &nbsp By selecting Accept, you confirm that you are authorized to act on your company’s behalf and you agree to the terms and conditions mentioned in the <a href="IceWarpPartnerAgreement.pdf">IceWarp Partner Network Agreement</a></label>
                </div>
              </div>
            </div>

            <!-- end term & condition -->
                  <!-- form group -->
          <div class="form-group row mt-3">
            <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm">Name<span class="required">*</span>: </label>
            <div class="col-sm-2">
              <input type="text" name="user_info[name]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Name" required="">
            </div>
            <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm">Place<span class="required">*</span>:</label>
            <div class="col-sm-2">
              <input type="text" name="user_info[place]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Place" required="">
            </div>
             <label for="colFormLabelSm" class="col-sm-1 col-form-label col-form-label-sm">Date<span class="required">*</span>:</label>
            <div class="col-sm-2">
              <input type="date" name="user_info[date]" class="form-control form-control-sm" id="colFormLabelSm" placeholder="Date" required="">
            </div>
          </div>
          <!-- end form group -->


         <!--  <div class="row mt-4">
              <div class="col-md-12">
                <div  class="checkbox">
                  <label><input type="checkbox" name="send_me_a_copy" >&nbsp &nbsp Send me a copy of my responses to my email</label>
                </div>
              </div>
            </div> -->

          <button type="submit" class="mt-4 mb-4 btn btn-primary btn-lg">Submit</button>

      </form>



      <form role="form">
     <!--   <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">
          <div class="form-group">
          <label>Legal Company Name</label>
          <input type="text" class="field" id="organization" placeholder="" name="organization">
        </div>
      </div>
        <div class="col-md-6">
          <label>Address&nbsp;</label>
          <input type="text" class="field" id="Address" placeholder="" name="Address"></div>
      </div> -->
      <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">City &nbsp;<input type="text" class="field" id="City" placeholder="" name="City"></div>
        <div class="col-md-6">Country&nbsp;<input type="text" class="field" id="Country" placeholder="" name="Country"></div>
      </div> -->
      <!-- end row -->
       <!-- row -->
     <!--  <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Company Telephone &nbsp;<input type="text" class="field" id="CompanyTelephone" placeholder="" name="CompanyTelephone"></div>
        <div class="col-md-6">Company Email&nbsp;<input type="email" class="field" id="CompanyEmail" placeholder="" name="CompanyEmail"></div> -->
      <!-- </div> -->
      <!-- end row -->
      <!-- row -->
    <!--   <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Website URL &nbsp;<input type="text" class="field" id="WebsiteURL" placeholder="" name="WebsiteURL"></div>
        <div class="col-md-6">Number of Locations&nbsp;<input type="text" class="field" id="NumberofLocations" placeholder="" name="NumberofLocations"></div> -->
      <!-- </div> -->
      <!-- end row -->
       <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Please specify if more than one &nbsp;<input type="text" class="field" id="ifmorethanone" placeholder="" name="ifmorethanone"></div>
        <div class="col-md-6">Other Sister Organizations&nbsp;<input type="text" class="field" id="OtherOrganizations" placeholder="" name="OtherOrganizations"></div> -->
      <!-- </div> -->
      <!-- end row -->
       <!-- row -->
     <!--  <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Contact Person Name &nbsp;<input type="text" class="field" id="ContactPersonName" placeholder="" name="ContactPersonName"></div>
        <div class="col-md-6">Mobile Number  &nbsp;<input type="text" class="field" id="MobileNumber " placeholder="" name="MobileNumber"></div> -->
      <!-- </div> -->
      <!-- end row -->
        <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Designation  &nbsp;<input type="text" class="field" id="Designation " placeholder="" name="Designation "></div>
        <div class="col-md-6">Email ID&nbsp;<input type="text" class="field" id="EmailID" placeholder="" name="EmailID"></div>
      </div> -->
      <!-- end row -->

      <!-- end company informtion -->
       
       <!-- <div class="text_blue mt-5">Company Registration Details</div> -->
       <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Trade License / Company Registration Number  &nbsp;<input type="text" class="field" id="TradeLicense " placeholder="" name="TradeLicense "></div>
        <div class="col-md-6">Date of Issue&nbsp;<input type="text" class="field" id="DateofIssue" placeholder="" name="DateofIssue"></div>
      </div> -->
      <!-- end row -->
      <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Expiry Date  &nbsp;<input  type="text" class="field" id="ExpiryDate " placeholder="" name="ExpiryDate "></div>
        <div class="col-md-6">Tax ID Number (if any) &nbsp;<input type="text" class="field" id="TaxID" placeholder="" name="TaxID"></div>
      </div> -->
      <!-- end row -->
      <!-- end Company Registration Details -->
      <!-- <div class="text_blue mt-5">Business Profile</div> -->
      <!-- <p style="font-size: 18px; margin-top: 10px; font-weight:600;">Fiscal Year Start</p> -->
       <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-4">Annual Revenue (Last Year)</div>
        <div class="col-md-4"><input  type="text" class="field" id="FiscalYear1" placeholder="in USD only" name="FiscalYear1"></div>
      </div> -->
        <!-- end row -->
        <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-4">Expected Annual Revenue (This Year)</div>
        <div class="col-md-4"><input type="text" class="field" id="FiscalYear2" placeholder="in USD only" name="FiscalYear2"></div>
      </div> -->
       <!-- end row -->
          <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-4">Revenue for Mailing Solutions (if any</div>
        <div class="col-md-4"><input type="text" class="field" id="FiscalYear3" placeholder="in USD only" name="FiscalYear3"></div>
      </div> -->
       <!-- end row -->
       <!-- end Business Profile -->
       <!-- <div class="text_blue mt-5">Primary Market Segments</div> -->
    <!--     <div class="row mt-4">
          <div class="col-md-12">
            <div  class="checkbox">
              <label><input type="checkbox" name="interest[]" value="SMB (less than 100 employees)">&nbsp &nbsp SMB (less than 100 employees)</label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="checkbox">
              <label><input type="checkbox" name="interest[]" value="Mid-Market (between 101 to 1000 employees)" >&nbsp &nbsp Mid-Market (between 101 to 1000 employees)</label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="checkbox">
              <label><input type="checkbox" name="interest[]" value="Enterprise (1,000+ Employees)" >&nbsp &nbsp Enterprise (1,000+ Employees)</label>
            </div>
          </div>
        </div> -->
        <!-- end Primary Market Segments -->
        <!-- <div class="text_blue mt-5">Primary Market Segments</div> -->
        <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Banking/Finance &nbsp;<input type="text" class="field" id="Banking/Finance" placeholder="" name="Banking/Finance"></div>
        <div class="col-md-6">Construction&nbsp;<input type="text" class="field" id="Construction" placeholder="" name="Construction"></div>
      </div> -->
      <!-- row -->
     <!--  <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Manufacturing &nbsp;<input  type="text" class="field" id="Manufacturing" placeholder="" name="Manufacturing"></div>
        <div class="col-md-6">IT/ITeS&nbsp;<input type="text" class="field" id="IT/ITeS" placeholder="" name="IT/ITeS"></div>
      </div> -->
      <!-- end row -->
       <!-- row -->
     <!--  <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Telecom/ISP &nbsp;<input type="text" class="field" id="Telecom/ISP" placeholder="" name="Telecom/ISP"></div>
        <div class="col-md-6">Education&nbsp;<input type="email" class="field" id="Education" placeholder="" name="Education"></div>
      </div> -->
      <!-- end row -->
      <!-- row -->
     <!--  <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Healthcare &nbsp;<input type="text" class="field" id="Healthcare" placeholder="" name="Healthcare"></div>
        <div class="col-md-6">Hospitality&nbsp;<input type="text" class="field" id="Hospitality" placeholder="" name="Hospitality"></div>
      </div> -->
      <!-- end row -->
       <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Retail &nbsp;<input type="text" class="field" id="Retail  " placeholder="" name="Retail  "></div>
        <div class="col-md-6">Government/Public Sector &nbsp;<input type="text" class="field" id="Government/PublicSector" placeholder="" name="Government/PublicSector"></div>
      </div> -->
      <!-- end row -->
       <!-- row -->
      <!-- <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">Others (Please specify &nbsp;<input type="text" class="field" id="Others" placeholder="" name="Others"></div>
        <div class="col-md-6"></div>
      </div> -->
      <!-- end row -->
      <!-- end Primary Market Segments -->
         <!-- <div class="text_blue mt-5">Existing Vendor Relationships (including any Mailing vendors) with % of revenue contribution</div> -->
          <!-- row -->
          <!--   <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="VendorName1" placeholder="Vendor Name 1" name="VendorName1 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="RevenueContributionv1" placeholder="Revenue Contribution % " name="RevenueContributionv1"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnerSince1" placeholder="Partner Since" name="PartnerSince1"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnershipLevel1" placeholder="Partnership Level" name="PartnershipLevel1"></div>
            </div> -->
          <!-- end row -->
            <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="VendorName2" placeholder="Vendor Name 2" name="VendorName2 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="RevenueContributionv2" placeholder="Revenue Contribution % " name="RevenueContributionv2"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnerSince2" placeholder="Partner Since" name="PartnerSince"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnershipLevel2" placeholder="Partnership Level" name="PartnershipLevel2"></div>
            </div> -->
          <!-- end row -->
            <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="VendorName3" placeholder="Vendor Name 3" name="VendorName3 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="RevenueContributionv3" placeholder="Revenue Contribution % " name="RevenueContributionv3"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnerSince3" placeholder="Partner Since" name="PartnerSince3"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnershipLevel3" placeholder="Partnership Level" name="PartnershipLevel3"></div>
            </div> -->
          <!-- end row -->
            <!-- row -->
          <!--   <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="VendorName4" placeholder="Vendor Name 4" name="VendorName4 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="RevenueContributionv4" placeholder="Revenue Contribution % " name="RevenueContributionv4"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnerSince4" placeholder="Partner Since" name="PartnerSince4"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnershipLevel4" placeholder="Partnership Level" name="PartnershipLevel4"></div>
            </div> -->
          <!-- end row -->
            <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="VendorName5" placeholder="Vendor Name 5" name="VendorName5 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="RevenueContributionv5" placeholder="Revenue Contribution % " name="RevenueContributionv5"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnerSince5" placeholder="Partner Since" name="PartnerSince5"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PartnershipLevel5" placeholder="Partnership Level" name="PartnershipLevel5"></div>
            </div> -->
          <!-- end row -->

        <!-- end Existing Vendor Relationships -->
            <!-- <div class="text_blue mt-5">Top 10 Customer Insights</div> -->
            <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer1" placeholder="Name of Customer 1" name="NameCustomer1 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince1" placeholder="Customer Since" name="CustomerSince1"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold1" placeholder="Solution Sold" name="SolutionSold1"></div> -->
            <!-- </div> -->
          <!-- end row --><!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer2" placeholder="Name of Customer 2" name="NameCustomer2 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince2" placeholder="Customer Since" name="CustomerSince2"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold2" placeholder="Solution Sold" name="SolutionSold2"></div> -->
            <!-- </div> -->
          <!-- end row --><!-- row -->
<!--             <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer3" placeholder="Name of Customer 31" name="NameCustomer3 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince3" placeholder="Customer Since" name="CustomerSince3"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold3" placeholder="Solution Sold" name="SolutionSold3"></div> -->
            <!-- </div> -->
          <!-- end row --><!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer4" placeholder="Name of Customer 4" name="NameCustomer4 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince4" placeholder="Customer Since" name="CustomerSince4"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold4" placeholder="Solution Sold" name="SolutionSold4"></div>
            </div> -->
          <!-- end row --><!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer5" placeholder="Name of Customer 5" name="NameCustomer5 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince5" placeholder="Customer Since" name="CustomerSince5"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold5" placeholder="Solution Sold" name="SolutionSold5"></div>
            </div> -->
          <!-- end row -->
          <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer6" placeholder="Name of Customer 6" name="NameCustomer6 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince6" placeholder="Customer Since" name="CustomerSince6"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold6" placeholder="Solution Sold" name="SolutionSold6"></div>
            </div> -->
          <!-- end row --><!-- row -->
        <!--     <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer7" placeholder="Name of Customer 7" name="NameCustomer7 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince7" placeholder="Customer Since" name="CustomerSince7"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold7" placeholder="Solution Sold" name="SolutionSold7"></div>
            </div> -->
          <!-- end row --><!-- row -->
          <!--   <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer8" placeholder="Name of Customer 8" name="NameCustomer8 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince8" placeholder="Customer Since" name="CustomerSince8"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold8" placeholder="Solution Sold" name="SolutionSold8"></div>
            </div> -->
          <!-- end row -->
          <!-- row -->
          <!--   <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer9" placeholder="Name of Customer 9" name="NameCustomer9 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince9" placeholder="Customer Since" name="CustomerSince9"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold9" placeholder="Solution Sold" name="SolutionSold9"></div>
            </div> -->
          <!-- end row -->
           <!-- row -->
          <!--   <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="NameCustomer10" placeholder="Name of Customer 10" name="NameCustomer10 "></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CustomerSince10" placeholder="Customer Since" name="CustomerSince10"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SolutionSold10" placeholder="Solution Sold" name="SolutionSold10"></div>
            </div> -->
          <!-- end row -->
          <!-- end top customer insights -->

          <!-- <div class="text_blue mt-5">Team Insights</div> -->
          <!-- <p style="font-size: 18px; margin-top: 10px; font-weight:600;">Total Number of Employees</p> -->

            <!-- <div class="row" style="margin-top: 20px;"> -->
            <!-- <div class="col-md-6">in Sales & Marketing &nbsp;<input style="width: 74%;" type="text" class="field" id="inSales&Marketing" placeholder="" name="inSales&Marketing"></div>
            <div class="col-md-6">in Technical&nbsp;<input style="width: 81%;" type="text" class="field" id="inTechnical" placeholder="" name="inTechnical"></div> -->
          <!-- </div> -->
          <!-- row -->
        <!--   <div class="row" style="margin-top: 20px;">
            <div class="col-md-6">in Administration &nbsp;<input style="width: 78%;" type="text" class="field" id="inAdministration" placeholder="" name="inAdministration"></div>
            <div class="col-md-6">Others&nbsp;<input style="width: 87%;" type="text" class="field" id="Others" placeholder="" name="Others"></div> -->
          <!-- </div> -->
          <!-- end row -->
          <!-- end Team Insights< -->

          <!-- <div class="text_blue mt-5">Contact Information</div> -->
          <!-- <p style="font-size: 18px; margin-top: 10px; font-weight:600;">Contact Type </p> -->
            <!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="CEOName" placeholder="Owner/MD/CEO Name" name="CEOName"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email1" placeholder="Email ID " name="email1"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber1" placeholder="Mobile Number" name="MobileNumber1"></div>
            </div> -->
          <!-- end row -->

          <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SalesHeadName" placeholder="Sales Head Name" name="SalesHeadName"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email2" placeholder="Email ID " name="email2"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber2" placeholder="Mobile Number" name="MobileNumber2"></div>
            </div> -->
          <!-- end row -->

          <!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="SalesManagerName" placeholder="Sales Manager Name" name="SalesManagerName"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email3" placeholder="Email ID " name="email3"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber3" placeholder="Mobile Number" name="MobileNumber3"></div>
            </div> -->
          <!-- end row -->
          <!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="PresalesManagerName" placeholder="Presales Manager Name" name="PresalesManagerName"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email4" placeholder="Email ID " name="email4"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber4" placeholder="Mobile Number" name="MobileNumber4"></div>
            </div> -->
          <!-- end row -->
          <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="TechnicalHeadName" placeholder="Technical Head Name" name="TechnicalHeadName"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email5" placeholder="Email ID " name="email5"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber5" placeholder="Mobile Number" name="MobileNumber5"></div>
            </div> -->
          <!-- end row -->
          <!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MarketingContact" placeholder="Marketing Contact" name="MarketingContact"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email6" placeholder="Email ID " name="email6"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber6" placeholder="Mobile Number" name="MobileNumber6"></div>
            </div> -->
          <!-- end row -->
          <!-- row -->
            <!-- <div class="row" style="margin-top: 20px;">
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="FinanceContact" placeholder="Finance Contact" name="FinanceContact"></div>
              <div class="col-md-3"><input style="width: 100%;" type="email" class="field" id="email7" placeholder="Email ID " name="email7"></div>
              <div class="col-md-3"><input style="width: 100%;" type="text" class="field" id="MobileNumber7" placeholder="Mobile Number" name="MobileNumber7"></div>
            </div> -->
          <!-- end row -->
          <!-- end Contact Information -->

           <!-- <div class="text_blue mt-5">IceWarp Relationship Interest</div> -->
            <!-- <div class="row" style="margin-top: 20px;"> -->
              <!-- <div class="col-md-12">
                <div  class="checkbox">
                  <label><input type="checkbox" name="interest[]" value="Sales Only">&nbsp &nbsp Sales Only</label>
                </div>
              </div> -->
             <!--  <div class="col-md-12">
                <div class="checkbox">
                  <label><input type="checkbox" name="interest[]" value="Sales, Implementation & Support" >&nbsp &nbsp Sales, Implementation & Support</label>
                </div>
              </div> -->
            <!-- </div> -->
        <!-- end IceWarp Relationship Interest -->

         <!-- <div class="text_blue mt-5">Territory (Country) request</div> -->
              <!-- row -->
              <!-- <div class="row" style="margin-top: 20px;"> -->
                <!-- <div class="col-md-6">Please specify &nbsp;<input style="width: 65%;" type="text" class="field" id="Territory" placeholder="" name="Territory"></div> -->
                <!-- <div class="col-md-6"></div> -->
              <!-- </div> -->
              <!-- end row -->
            <!-- end Territory (Country) request -->
        <!-- <div class="text_blue mt-5">Attachments</div> -->
              <!-- row -->
              <!-- <div class="row mt-2"> -->
                <!-- <div class="col-md-3"> -->
                <!-- <label for="file-upload" class="custom-file-upload">Company Registration Document</label> -->
              <!-- </div>
              <div class="col-md-6">
                <input id="file-upload" type="file"/>
              </div>
              </div>
 -->
              <!-- end row -->
               <!-- row -->
              <!-- <div class="row mt-2">
                 <div class="col-md-3">
                  <label for="file-upload" class="custom-file-upload">Tax Certificate</label>
                </div>
                <div class="col-md-6">
                  <input id="file-upload" type="file"/>
                </div>
              </div> -->

              <!-- end row -->
               <!-- row -->
             <!--  <div class="row mt-2">
                <div class="col-md-3">
                  <label for="file-upload" class="custom-file-upload">Any Other</label>
                </div>
                <div class="col-md-6">
                  <input id="file-upload" type="file"/>
                </div>
              </div> -->
              <!-- end row -->
            <!-- end Attachments -->
           <!--  <div class="row mt-4">
              <div class="col-md-12">
                <div  class="checkbox">
                  <label><input type="checkbox" name="interest[]" value="I confirm that the above provided information is accurate and errors, if any, are mine.">&nbsp &nbsp I confirm that the above provided information is accurate and errors, if any, are mine. <a href="IceWarpPartnerAgreement.pdf">  Tearms & Conditions</a></label>
                </div>
              </div>
            </div> -->
        <!-- end IceWarp Relationship Interest -->

        <!-- row -->
           <!--  <div class="row" style="margin-top: 20px;">
              <div class="col-md-3">Name: &nbsp;<input style="width: 70%;" type="text" class="field" id="Name" placeholder="" name="Name"></div>
              <div class="col-md-3">Place: &nbsp;<input style="width: 70%;" type="text" class="field" id="place" placeholder="" name="place"></div>
              <div class="col-md-3">Date: &nbsp;<input style="width: 70%;" type="text" class="field" id="Date" placeholder="" name="Date"></div>
            </div> -->
          <!-- end row -->
        <!--    <div class="row" style="margin-top: 50px;">
              <button type="submit" class="btn btn-primary btn-lg">Submit</button>
           </div> -->
          </form>
    </div>


        

    <!--   <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Company Name</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
      </footer> -->
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-3.2.1.slim.min.js" integrity="s" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/vendor/holder.min.js"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
    <style type="text/css">
      .required{
        color: red;
      }
    </style>
  </body>
</html>
